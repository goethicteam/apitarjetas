'use strict';

var mongoose = require('mongoose'),
  card = mongoose.model('Card');


exports.list_all_cards = function(req, res) {
  card.find({userId: req.get('UserID')}, function(err, card) {
    if (err)
      res.send(err);
    res.json(card);
  });
};


exports.create_a_card = function(req, res) {
  var new_card = new card(req.body);
  new_card.save(function(err, card) {
    if (err)
      res.send(err);
    res.json(card);
  });
};

exports.read_a_card = function(req, res) {
  card.find({id : req.params.id}, function(err, card) {
    if (err)
      res.send(err);
    res.json(card);
  });
};

exports.update_a_card = function(req, res) {
  card.findOneAndUpdate({id:req.params.id}, req.body, {new: true}, function(err, cards) {
    if (err)
      res.send(err);
    res.json(card);
  });
};

exports.delete_a_card = function(req, res) {
  card.remove({
    id: req.params.id
  }, function(err, card) {
    if (err)
      res.send(err);
    res.json({ message: 'card successfully deleted' });
  });
};


exports.add_a_transaction = function(req,res) {
  card.update({id:req.params.id}, {'$addToSet':{"cardTransactions":req.body}}, function(err, card){
    if (err)
      res.send(err);
    res.json(card);
  });
};


// exports.add_a_transacction = function(req, res) {
//   var transaction = new card(req.body);
//   card.findOne({id: req.params.id}, function(err, card) {
//     card.transacctions.push({
//       id: req.body.id,
//       type: "dd",
//       description: "dd",
//       currency: "ee"
//     });
//     if (err)
//       res.send(err);
//     res.json(card);
//   });
// };
