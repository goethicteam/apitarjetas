'use strict';

module.exports = function(app) {
	var cards = require('../controllers/cardsController');

	// todoList Routes
	app.route('/cards')
		.get(cards.list_all_cards)
		.post(cards.create_a_card);

	app.route('/cards/:id')
		.get(cards.read_a_card)
		.put(cards.update_a_card)
		.delete(cards.delete_a_card)
		.patch(cards.add_a_transaction);


};
