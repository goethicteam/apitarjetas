var http = require('http');
var express = require('express');
var cors = require('cors'),
  app = express(),
  port = process.env.PORT || 3002,
  mongoose = require('mongoose'),
  customers = require('./api/models/cardsModel'),
  bodyParser = require('body-parser');
app.use(cors());
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://ec2-52-210-105-124.eu-west-1.compute.amazonaws.com/local');
var user;
var resp;
var myLogger = function (req, res, next) {

  // / create the JSON object
  jsonObject = JSON.stringify({
      "message" : "The web of things is approaching, let do some tests to be ready!",
      "name" : "Test message posted with node.js",
      "caption" : "Some tests with node.js",
      "link" : "http://www.youscada.com",
      "description" : "this is a description",
      "picture" : "http://youscada.com/wp-content/uploads/2012/05/logo2.png",
      "actions" : [ {
          "name" : "youSCADA",
          "link" : "http://www.youscada.com"
      } ]
  });

  // prepare the header
  var postheaders = {
      'Content-Type' : 'application/x-www-form-urlencoded',
      'Authorization' : req.get('Authorization'),
      'UserID': req.get('UserID')
  };

  // the post options
  var optionspost = {
      host : 'localhost',
      port : 3003,
      path : '/restrictedArea/enter',
      method : 'POST',
      headers : postheaders
  };

  console.info('Options prepared:');
  console.info(optionspost);
  console.info('Do the POST call');

  // do the POST call
  var reqPost = http.request(optionspost, function(resp) {
      console.log("statusCode: ", resp.statusCode);
      // uncomment it for header details
  //  console.log("headers: ", res.headers);

      resp.on('data', function(d) {

          if(resp.statusCode===200){
            var responseData = JSON.parse(d);
            console.log(responseData.user);
            next();
          }
          else{
            res
      .status(403)
      .json({
           'message': 'Invalid',
      });
          }

      });

  });

  // write the js
  reqPost.write(jsonObject);
  reqPost.end();
  reqPost.on('error', function(e) {
      console.error(e);
  });



};

app.use(myLogger);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/cardsRoutes');
routes(app);

//midleware
app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});

app.listen(port);

console.log('cards RESTful API server started on: ' + port);
